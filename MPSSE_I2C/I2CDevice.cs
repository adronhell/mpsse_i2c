﻿using System;
using System.Collections.Generic;
using System.Linq;
using FTD2XX_NET;

namespace ft2xxi2c
{
    public class I2CDevice
    {
        protected int Speed = 60;
        protected byte SavedPortValue;
        private FTDI ftdi;
        protected List<byte> OutputBuffer = new List<byte>();

        protected I2CDevice(FTDI ftdi1)
        {
            ftdi = ftdi1;
        }

        private bool OpenPort(string portName)
        {
            var status = ftdi.OpenByDescription(portName);
            CheckStatus(status);
            return true;
        }

        private static void CheckStatus(FTDI.FT_STATUS status)
        {
            if (status != FTDI.FT_STATUS.FT_OK)
            {
                throw new ApplicationException("Received result: " + status.ToString());
            }
        }

        public string[] ListDevs()
        {
            uint devCount = 0;
            var status = ftdi.GetNumberOfDevices(ref devCount);
            CheckStatus(status);
            Console.WriteLine("Found {0} devices.", devCount);
            if (devCount == 0)
                return new string[] {};
            var infonodes = new FTDI.FT_DEVICE_INFO_NODE[devCount];
            status = ftdi.GetDeviceList(infonodes);
            CheckStatus(status);
            return infonodes.Select(x => x.Description).ToArray();
        }

        public void ClosePort()
        {
            var status = ftdi.Close();
            CheckStatus(status);
        }

        private void AddToBuffer(byte value)
        {
            OutputBuffer.Add(value);
        }

        protected void SendBytes()
        {
            uint i = 0;
            var status = ftdi.Write(OutputBuffer.ToArray(), OutputBuffer.Count, ref i);
            CheckStatus(status);
            OutputBuffer.Clear();
        }

        //
        // This will work out the number of whole bytes to read
        //
        private byte[] Read_Data(ushort bitCount)
        {
            uint j = 0;

            //var tempBuffer = new byte[64001];
            var ftInBuffer = new byte[64001];
            var modBitCount = bitCount - 1;
            var noBytes = modBitCount/8;
            var bitShift = modBitCount%8;
            if (bitShift > 0) noBytes = noBytes + 1; // bump whole bytes if bits left over
            var result = new byte[bitCount/8 + ((bitCount%8 > 0) ? 1 : 0)];
            int totalBytes = 0;
            uint ftQBytes = 0;
            do
            {
                FTDI.FT_STATUS res;
                do
                {
                    res = ftdi.GetRxBytesAvailable(ref ftQBytes);
                    CheckStatus(res);
                } while (ftQBytes == 0);
                res = ftdi.Read(ftInBuffer, ftQBytes, ref j);
                CheckStatus(res);
                for (int i = 0; i <= (j - 1); i++)
                {
                    result[totalBytes] = ftInBuffer[i];
                    totalBytes = totalBytes + 1;
                }
            } while (totalBytes < noBytes);

            return result.ToArray();
        }

        //
        // This should satisfy outstanding commands.
        //
        // We will use $AA and $AB as commands which
        // are invalid so that the MPSSE block should echo these
        // back to us preceded with an $FA
        //
        private bool Sync_To_MPSSE()
        {
            
            byte[] ftInBuffer;
            uint i = 0;
            uint ftQBytes = 0;
            var result = false;
            var res = ftdi.GetRxBytesAvailable(ref ftQBytes);
            CheckStatus(res);
            if ((ftQBytes > 0))
            {
                ftInBuffer = new byte[ftQBytes];
                ftdi.Read(ftInBuffer, ftQBytes, ref ftQBytes);
            }
            do
            {
                OutputBuffer.Clear();
                AddToBuffer(0xAA); // bad command
                SendBytes();
                res = ftdi.GetRxBytesAvailable(ref ftQBytes);
                CheckStatus(res);
            } while (!((ftQBytes > 0) || (res != FTDI.FT_STATUS.FT_OK))); // or timeout
            
            ftInBuffer = new byte[ftQBytes];
            ftdi.Read(ftInBuffer, ftQBytes, ref i);
            int j = 0;
            bool done = false;
            do
            {
                if ((ftInBuffer[j] == 0xFA))
                {
                    if ((j <= (i - 2)))
                    {
                        if ((ftInBuffer[j + 1] == 0xAA))
                        {
                            done = true;
                        }
                    }
                }
                j = j + 1;
            } while (!((j == i) || done));
            OutputBuffer.Clear();
            AddToBuffer(0xAB); // bad command
            SendBytes();
            do
            {
                res = ftdi.GetRxBytesAvailable(ref ftQBytes);
            } while (!((ftQBytes > 0) || (res != FTDI.FT_STATUS.FT_OK))); // or timeout
            CheckStatus(res);
            ftInBuffer = new byte[ftQBytes];
            ftdi.Read(ftInBuffer, ftQBytes, ref i);
            j = 0;
            done = false;
            do
            {
                if ((ftInBuffer[j] == 0xFA))
                {
                    if ((j <= (i - 2)))
                    {
                        if ((ftInBuffer[j + 1] == 0xAB))
                        {
                            done = true;
                        }
                    }
                }
                j = j + 1;
            } while (!((j == i) || done));

            if (done) result = true;
            return result;
        }

        //----------------------------------
        //  I2C Routines
        //----------------------------------

        protected bool Set_Start()
        {
            var result = false;
            SavedPortValue = (byte) (SavedPortValue | 0x03); //SCL SDA high
            AddToBuffer(0x80);
            AddToBuffer(SavedPortValue);
            AddToBuffer(0x13); // set SCL,SDA,WP as out
            SavedPortValue = (byte) (SavedPortValue & 0xFD); //SCL high SDA low
            AddToBuffer(0x80);
            AddToBuffer(SavedPortValue);
            AddToBuffer(0x13); // set SCL,SDA,WP as out
            SavedPortValue = (byte) (SavedPortValue & 0xFC); //SCL low SDA low
            AddToBuffer(0x80);
            AddToBuffer(SavedPortValue);
            AddToBuffer(0x13); // set SCL,SDA,WP as out
            result = true;
            return result;
        }

        protected bool Set_Stop()
        {
            var result = false;
            SavedPortValue = (byte) (SavedPortValue | 0x01); //SCL high SDA low
            SavedPortValue = (byte) (SavedPortValue & 0xFD); //SCL high SDA low
            AddToBuffer(0x80);
            AddToBuffer(SavedPortValue);
            AddToBuffer(0x13); // set SCL,SDA,WP as out
            SavedPortValue = (byte) (SavedPortValue | 0x02); //SCL high SDA high
            AddToBuffer(0x80);
            AddToBuffer(SavedPortValue);
            AddToBuffer(0x13); // set SCL,SDA,WP as out
            // tristate SDA SCL
            AddToBuffer(0x80);
            AddToBuffer(SavedPortValue);
            AddToBuffer(0x10); // set SCL,SDA as input,WP as out
            result = true;
            return result;
        }

        public bool ScanIn_Out(int bitCount, byte[] outBuffPtr)
        {
            var j = 0;
            // adjust count value
            var modBitCount = bitCount - 1;
            int i;
            if (modBitCount/8 > 0)
            {
// do whole bytes
                i = (modBitCount/8) - 1;
                AddToBuffer(0x35); // clk data bytes out on -ve in -ve clk MSB
                AddToBuffer((byte) (i & 0xFF));
                AddToBuffer((byte) ((i/256) & 0xFF));
                // now add the data bytes to go out
                do
                {
                    AddToBuffer(outBuffPtr[j]);
                    j = j + 1;
                } while (j <= i);
            }
            if (modBitCount%8 > 0)
            {
// do remaining bits
                i = (modBitCount%8);
                AddToBuffer(0x37); // clk data bits out on -ve in -ve clk MSB
                AddToBuffer((byte) (i & 0xFF));
                // now add the data bits to go out
                AddToBuffer(outBuffPtr[j]);
            }
            return false;
        }

        private bool ScanIn(int bitCount)
        {
            // adjust count value
            int modBitCount = bitCount - 1;
            if (modBitCount == 0)
            {
                AddToBuffer(0x27); // clk data bits in -ve clk MSB
                AddToBuffer(0);
            }
            else
            {
                int i;
                if (modBitCount/8 > 0)
                {
                    // do whole bytes
                    i = (modBitCount/8) - 1;
                    AddToBuffer(0x25); // clk data bytes in -ve clk MSB
                    AddToBuffer((byte) (i & 0xFF));
                    AddToBuffer((byte) ((i/256) & 0xFF));
                }
                if (modBitCount%8 > 0)
                {
                    // do remaining bits
                    i = (modBitCount%8);
                    AddToBuffer(0x27); // clk data bits in -ve clk MSB
                    AddToBuffer((byte) (i & 0xFF));
                }
            }
            return false;
        }

        private bool ScanOut(int bitCount, byte[] outBuffPtr)
        {
            var j = 0;
            // adjust count value
            int modBitCount = bitCount - 1;
            int i;
            if (modBitCount/8 > 0)
            {
                // do whole bytes
                i = (modBitCount/8) - 1;
                AddToBuffer(0x11); // clk data bytes out on -ve MSB
                AddToBuffer((byte) (i & 0xFF));
                AddToBuffer((byte) ((i/256) & 0xFF));
                // now add the data bytes to go out
                do
                {
                    AddToBuffer(outBuffPtr[j]);
                    j = j + 1;
                } while (j <= i);
            }
            if (modBitCount%8 > 0)
            {
                // do remaining bits
                i = (modBitCount%8);
                AddToBuffer(0x13); // clk data bits out on -ve MSB
                AddToBuffer((byte) (i & 0xFF));
                // now add the data bits to go out
                AddToBuffer(outBuffPtr[j]);
            }
            return false;
        }

        protected bool Send_Byte_Then_Chk_ACK(byte data)
        {
            var result = false;
            ScanOut(8, new[] {data});
            AddToBuffer(0x80);
            AddToBuffer(SavedPortValue);
            AddToBuffer(0x11); // set SCL,WP as out SDA as in
            ScanIn(1);
            AddToBuffer(0x87); //Sendimmediate
            SendBytes(); // Sendoff the command
            var inBuff = Read_Data(1);
            var i = inBuff[0] & 0x01;
            if ((i == 0))
                result = true;
            AddToBuffer(0x80);
            AddToBuffer(SavedPortValue);
            AddToBuffer(0x13); // set SCL,SDA,WP as out
            return result;
        }

        public bool Send_Byte_Then_Ignore_ACK(byte data)
        {
            bool passed = ScanOut(8, new[] {data});
            if (passed)
            {
                AddToBuffer(0x80);
                AddToBuffer(SavedPortValue);
                AddToBuffer(0x11); // set SCL,WP as out SDA as in
                ScanOut(1, new byte[]{0});
                AddToBuffer(0x80);
                AddToBuffer(SavedPortValue);
                AddToBuffer(0x13); // set SCL,SDA,WP as out
            }
            return true;
        }

        protected bool Read_Byte_Then_Chk_ACK(ref byte Data)
        {
            var result = false;
            AddToBuffer(0x80);
            AddToBuffer(SavedPortValue);
            AddToBuffer(0x11); // set SCL,WP as out SDA as in
            ScanIn(9);
            AddToBuffer(0x87); //Send immediate
            SendBytes(); // send off the command
            var inBuff = Read_Data(9);
            Data = inBuff[0];
            int i = inBuff[1] & 0x01;
            if ((i == 0))
                result = true;
            AddToBuffer(0x80);
            AddToBuffer(SavedPortValue);
            AddToBuffer(0x13); // set SCL,SDA,WP as out
            return result;
        }

        private void Log(string log)
        {
            Console.WriteLine(log);
        }

        public bool Init_Controller(String dName)
        {
            var result = false;
            var passed = OpenPort(dName);
            Log(string.Format("OpenPort({0}): {1}", dName, passed));
            if (passed)
            {
                var res = ftdi.SetLatency(16);
                CheckStatus(res);
                res = ftdi.SetBitMode(0x00, FTDI.FT_BIT_MODES.FT_BIT_MODE_RESET);
                CheckStatus(res);
                res = ftdi.SetBitMode(0x00, FTDI.FT_BIT_MODES.FT_BIT_MODE_MPSSE);
                CheckStatus(res);
                passed = Sync_To_MPSSE();
                Log(string.Format("Sync_To_MPSSE: {0}", passed));
            }
            if (passed)
            {
                OutputBuffer.Clear();
                //  sleep(20); // wait for all the USB stuff to complete
                AddToBuffer(0x80); // set SK,DO,CS as out
                AddToBuffer(0x13); // SDA SCL WP high
                SavedPortValue = 0x13;
                AddToBuffer(0x13); // inputs on GPIO12-14
                AddToBuffer(0x82); // outputs on GPIO21-24
                AddToBuffer(0x0F);
                AddToBuffer(0x0F);
                AddToBuffer(0x86); // set clk divisor
                AddToBuffer((byte) (Speed & 0xFF));
                AddToBuffer((byte) (Speed >> 8));
                // turn off loop back
                AddToBuffer(0x85);
                SendBytes(); // Sendoff the command
                result = true;
            }
            return result;
        }
    }
}