using System;
using System.Collections.Generic;
using FTD2XX_NET;

namespace ft2xxi2c
{
    public class Mem24C02 : I2CDevice
    {
        public Mem24C02(FTDI ftdi1) : base(ftdi1)
        {
        }

        public int Read_Location(int LocAddress, int DevAddress)
        {
            bool passed, ACK;
            int i;
            byte Data;
            var result = 0;
            OutputBuffer.Clear();
            passed = Set_Start();
            // set up read address
            i = ((DevAddress << 1) & 0xFE);
            ACK = Send_Byte_Then_Chk_ACK((byte)i);
            ACK = Send_Byte_Then_Chk_ACK((byte)LocAddress);
            passed = Set_Start();
            // now read data
            i = ((DevAddress << 1) & 0xFE) | 0x01;
            ACK = Send_Byte_Then_Chk_ACK((byte)i);
            Data = 0;
            ACK = Read_Byte_Then_Chk_ACK(ref Data);

            passed = Set_Stop();
            SendBytes(); // s}off the command
            result = Data;
            return result;
        }

        public bool Write_Location(int LocAddress, int DevAddress, int data)
        {
            bool passed, ACK;
            int i;
            byte dbyte;
            var result = false;
            OutputBuffer.Clear();
            SavedPortValue = (byte)(SavedPortValue & 0xEF); //WP low
            do
            {
                passed = Set_Start();
                // set up device address
                i = ((DevAddress << 1) & 0xFE);
                ACK = Send_Byte_Then_Chk_ACK((byte)i);
            } while (!ACK);
            // set up byte address
            ACK = Send_Byte_Then_Chk_ACK((byte)LocAddress);
            // now write data
            dbyte = (byte)(data & 0xFF);
            ACK = Send_Byte_Then_Chk_ACK(dbyte);
            passed = Set_Stop();
            SendBytes(); // s}off the command
            SavedPortValue = (byte)(SavedPortValue | 0x10); //WP high

            do
            {
                passed = Set_Start();
                // set up write address
                i = ((DevAddress << 1) & 0xFE);
                ACK = Send_Byte_Then_Chk_ACK((byte)i);
            } while (!ACK);

            result = true;
            return result;
        }

        public bool Erase_24C02(String DName, int DevAddress)
        {
            bool passed;
            int address, i;
            var result = false;
            passed = Init_Controller(DName);
            if (passed)
            {
                for (i = 0; i <= 0x7f; i++)
                {
                    passed = Write_Location(i, DevAddress, 0xFF);
                }
                ClosePort();
                result = true;
            }
            return result;

        }

        public uint[] Read_24C02(string dName, int devAddress)
        {
            bool passed;
            int i, j;
            passed = Init_Controller(dName);
            var res = new List<uint>();
            if (passed)
            {
                for (j = 0; j <= 0x7F; j++)
                {
                    i = Read_Location(j, devAddress);
                    res.Add((uint)(i & 0xFFFF));
                }
                ClosePort();
            }
            return res.ToArray();
        }
    }
}