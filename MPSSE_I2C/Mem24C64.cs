using System;
using FTD2XX_NET;

namespace ft2xxi2c
{
    public class Mem24C64 : I2CDevice
    {
        public Mem24C64(FTDI ftdi1) : base(ftdi1)
        {
            //60 - 100kHz
            // 15 - 375kHz
            // 2000 - 6kHz
            Speed = 2000;
        }
        public int Read_Location(UInt16 locAddress, byte devAddress)
        {
            byte Data;
            OutputBuffer.Clear();
            Set_Start();
            // set up read address
            var i = ((devAddress << 1) & 0xFE);
            Send_Byte_Then_Chk_ACK((byte)i);
            Send_Byte_Then_Chk_ACK((byte)(locAddress >> 8));
            Send_Byte_Then_Chk_ACK((byte)(locAddress & 0xFF));
            Set_Start();
            // now read data
            i = ((devAddress << 1) & 0xFE) | 0x01;
            Send_Byte_Then_Chk_ACK((byte)i);
            Data = 0;
            Read_Byte_Then_Chk_ACK(ref Data);

            Set_Stop();
            SendBytes(); // s}off the command
            return Data;
        }

        public bool Write_Location(UInt16 locAddress, byte devAddress, byte data)
        {
            bool ack;
            OutputBuffer.Clear();
            SavedPortValue = (byte)(SavedPortValue & 0xEF); //WP low
            do
            {
                Set_Start();
                // set up device address
                //int i = 0xA0;
                var i = ((devAddress << 1) & 0xFE);
                ack = Send_Byte_Then_Chk_ACK((byte)i);
            } while (!ack);
            // set up byte address
            Send_Byte_Then_Chk_ACK((byte)(locAddress >> 8));
            Send_Byte_Then_Chk_ACK((byte)(locAddress & 0xFF));
            // now write data
            Send_Byte_Then_Chk_ACK(data);
            Set_Stop();
            SendBytes(); // s}off the command
            SavedPortValue = (byte)(SavedPortValue | 0x10); //WP high

            return true;
        }
    }
}