﻿using System;
using System.Threading;
using FTD2XX_NET;

namespace ft2xxi2c
{
    class Program
    {

        private const byte devAddr = 0x50;
        static void Main(string[] args)
        {
            var i2CWrapper = new Mem24C64(new FTDI());
            var devs = i2CWrapper.ListDevs();

            bool passed = i2CWrapper.Init_Controller(devs[0]);
            if (passed)
            {
                Console.WriteLine("Start reading {0}", DateTime.Now);
                for (int i = 0; i < 100; i++)
                {
                    Console.WriteLine("Write: {0:X2}", i);
                    i2CWrapper.Write_Location((ushort) i, devAddr, (byte) i);
                    Thread.Sleep(20);
                    var readLocation = i2CWrapper.Read_Location((ushort)i, devAddr);
                    Console.WriteLine("Read: {0:X2}", readLocation);
                }

                i2CWrapper.ClosePort();
                Console.WriteLine("Reading done {0}", DateTime.Now);
            }
            else
            {
                Console.WriteLine("Can't open device.");
            }
            Console.ReadKey(true);
        }

        private static void CheckStatus(FTDI.FT_STATUS status)
        {
            if (status != FTDI.FT_STATUS.FT_OK)
            {
                throw new ApplicationException("Received result: " + status.ToString());
            }
        }
    }
}
